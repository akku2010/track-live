import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProfilePage } from './profile';
import { IonPullupModule } from 'ionic-pullup';
import { TranslateModule } from '@ngx-translate/core';
import { Crop } from '@ionic-native/crop';
@NgModule({
  declarations: [
    ProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(ProfilePage),
    IonPullupModule,
    TranslateModule.forChild()
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [Crop]
})
export class ProfilePageModule {}
